
Reg_Stat.controller('main_controller', ['$scope', '$injector', '$http', '$location', function($scope, $injector, $http, $location) {

        $scope.rightMenu = {};
        $scope.mainWindow = {};
        $scope.tabs = {};
        $scope.feed = {};

        $scope.showErrorServer = function(status, response) {
            response = response === undefined ? false : response;
            if ((response && response.error !== undefined) || /[4-5][0-9][0-9]/.test(status)) {
                var serviceAnimate = $injector.get('$animate');
                var popup_error = angular.element('.popup_error__unvisible');
                popup_error.addClass('popup_error');
                popup_error.removeClass('popup_error__unvisible');
                if (response && response.error !== undefined) {
                    $scope.error.errorText = response.error;
                }
                else {
                    $scope.error.errorText = 'Сервер недоступен. Попробуйте позже или свяжитесь с системным администратором.';
                }
                setTimeout(function() {
                    serviceAnimate.animate(popup_error, {opacity: 1}, {opacity: 0}).then(function() {
                        setTimeout(function() {
                            /*это печально*/
                            popup_error.addClass('popup_error__unvisible');
                            popup_error.removeClass('popup_error');
                            popup_error.css({opacity: 1});
                        }, 1100);
                    });
                }, 1000);
                return false;
            }
            return true;
        };

        $scope.tabs.getTabs = function(page) {
            page = page === undefined ? 'login' : page;
            switch (page) {
                case 'login':
                    return [{title: 'Вход', class: 'active'}];
                case 'feed':
                    return [{title: 'Лента', class: 'active', page: 'feed'}, {title: 'Форма', page: 'formP1'}];
                case 'formP1':
                    return [{title: 'Лента', page: 'feed'}, {title: 'Форма', class: 'active', page: 'formP1'}];
            }
        };

        $scope.tabs.getPage = function(page) {
            page = page === undefined ? false : page;
            $http.get('getPage', {params: {page: page}}).success(function(result) {
                switch (result.page) {
                    case 'feed':
                        $scope.mainWindow.switch = 'feed';
                        $scope.tabs.tabsArray = $scope.tabs.getTabs('feed');
                        $scope.rightMenu.FIO = result.FIO;
                        $location.path('/feed');
                        break;
                    case 'formP1':
                        $scope.mainWindow.switch = 'formP1';
                        $scope.tabs.tabsArray = $scope.tabs.getTabs('formP1');
                        $scope.rightMenu.FIO = result.FIO;
                        $location.path('/formP1');
                        break;
                    default:
                        $scope.mainWindow.switch = 'formEnter';
                        $scope.tabs.tabsArray = $scope.tabs.getTabs();
                        $scope.rightMenu.FIO = false;
                        $location.path('');
                }
            });
        };

        $scope.tabs.getPage();
        
        $scope.rightMenu.logout = function() {
            $http.get('/logout').success(function(result, status) {
                if ($scope.showErrorServer(status, result) && result) {
                    $scope.mainWindow.switch = 'formEnter';
                    $scope.tabs.tabsArray = $scope.tabs.getTabs();
                    $scope.rightMenu.FIO = false;
                    $location.path('');
                }
                ;
            });
        };

    }]);


Reg_Stat.controller('formEnter_controller', ['$http', '$scope', '$cookies', '$location', function($http, $scope, $cookies, $location) {

        $scope.formEnter = {};

        $scope.formEnter.login = $cookies.loginName;

        $scope.formEnter.checkLogin = function() {

            /[а-яА-ЯёЁ]+/.test(this.login)
                    ? this.russianLogin = true
                    : this.russianLogin = false;
            /[-!"#$%&'()*+,./:;<=>?@[\\\]_`{|}~]+/.test(this.login)
                    ? this.nonLetterLogin = true
                    : this.nonLetterLogin = false;

        };

        $scope.formEnter.checkPassword = function() {

            /[а-яА-ЯёЁ]+/.test(this.password)
                    ? this.russianPassword = true
                    : this.russianPassword = false;
            /[-!"#$%&'()*+,./:;<=>?@[\\\]_`{|}~]+/.test(this.password)
                    ? this.nonLetterPassword = true
                    : this.nonLetterPassword = false;

        };

        $scope.formEnter.sendLogin = function() {

            if (!(this.russianLogin ||
                    this.russianPassword ||
                    this.nonLetterLogin ||
                    this.nonLetterPassword)) {

                $http.get('getLogin', {params: {login: this.login}}).success(function(response, status) {
                    if ($scope.showErrorServer(status, response)) {
                        var hash = Tea.decrypt(response, calcMD5($scope.formEnter.password));
                        $http.get('getAccess', {params: {login: $scope.formEnter.login, hash: hash}}).success(function(response, status) {
                            if ($scope.showErrorServer(status, response)) {
                                $scope.tabs.tabsArray = $scope.tabs.getTabs('feed');
                                $scope.mainWindow.switch = 'feed';
                                $scope.feed.notes = [{}];
                                $scope.rightMenu.FIO = response.FIO;
                                $location.path('/feed');                                         
                            }
                        });
                    }
                });
            }

        };


    }]);

Reg_Stat.controller('feed_controller', ['$scope', function($scope) {

    }]);