Reg_Stat.config(['$httpProvider', '$locationProvider', function($httpProvider, $locationProvider) {
              
        $locationProvider.html5Mode(true);
        
        $httpProvider.interceptors.push(['$animate', function($animate) {                             
                
                var container = angular.element('.container');
                var popup_load = angular.element('.popup_load__unvisible');

                return {
                    'request': function(config) {

                        $animate.addClass(container, 'container_main__hide');
                        $animate.removeClass(popup_load, 'popup_load__unvisible');
                        $animate.addClass(popup_load, 'popup_load__visible');

                        return config;
                    },
                    'response': removePopupLoad,
                    'responseError': removePopupLoad

                };

                function removePopupLoad(response) {

                    $animate.removeClass(container, 'container_main__hide');
                    $animate.removeClass(popup_load, 'popup_load__visible');
                    $animate.addClass(popup_load, 'popup_load__unvisible');

                    return response;
                }

            }]);
    }]);
