var express = require('express'),
        cookieParser = require('cookie-parser'),
        app = express(),
        session = require('express-session'),
        MongoStore = require('connect-mongo')(session),
        config = require('./configuration/configurationServer.json'),
        http = require('http'),
        html_path = __dirname + '/public/html/',
        Tea = require('./protected/js/Tea.js'),
        mongoose = require('mongoose'),
        MD5 = require('./protected/js/md5.js');


function throwMyError(res, message) {
    if (res.send !== undefined && res.send) {
        message = message !== undefined && message ? message : 'Ошибка!';
        res.send({error: message});
    }
}

mongoose.connect('mongodb://localhost:27017/regstatangular');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

var schemaModelUsers = new mongoose.Schema({login: 'String',
    password: 'String',
    FIO: 'String',
    successLogin: 'Boolean',
    key: 'String',
    keyDate: 'Date'},
{collection: 'users'});

var modelUsers = mongoose.model('user', schemaModelUsers);

app.use(express.static('public'));
app.use(cookieParser());

app.use(session({
    name: 'sess',
    secret: 'somesecret',
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 600000
    },
    store: new MongoStore({mongooseConnection: db})
}));

app.get('/', function(req, res) {
    res.sendFile(html_path + 'index.html');
});

app.get('/getLogin', function(req, res) {
    /*получить хэш*/
    if (req.query.login === undefined || req.query.login.toString().length < 1) {
        throwMyError(res, 'Неправильный логин или пароль!');
        return false;
    }

    modelUsers.findOne({login: req.query.login}, function(err, result) {
        if (result === null) {
            res.send(MD5.calcMD5((Math.random() * 1000000) + ''));
            return false;
        }
        result.key = ((Math.random() * 1000000) + '')
                .base64Encode();
        result.keyDate = new Date();
        result.save();
        res.send(
                Tea.encrypt(
                        result.key,
                        result.password)
                );
    });
});


app.get('/getAccess', function(req, res) {
    if (req.query.login === undefined || req.query.login.toString().length === 0 ||
            req.query.hash === undefined || req.query.hash.toString().length === 0) {

        throwMyError(res, 'Неправильный логин или пароль!');
        return false;

    }

    modelUsers.findOne({login: req.query.login}, function(err, result) {
        if (result === null || new Date() - result.keyDate > 20000 ||
                result.key !== req.query.hash.toString()) {
            throwMyError(res, 'Неправильный логин или пароль!');
            return false;
        }

        res.cookie('loginName', req.query.login, {expires: new Date(Date.now() + 2678400000)});

        req.session.login = result.login;
        req.session.page = 'feed';
        req.session.FIO = result.FIO;

        req.session.save();

        res.send({FIO: result.FIO});
    });

});

app.get('/getPage', function(req, res) {
    var switchPage;
    switchPage = req.query.page && req.session.page ?
            req.query.page :  req.session.page;
            
    switch (switchPage) {
        case 'feed':
            res.send({page: 'feed', FIO: req.session.FIO});
            break;
        case 'formP1':
            res.send({page: 'formP1', FIO: req.session.FIO});
            break;
        default:
            res.send({page: 'formEnter'});
    }
});

app.get('/logout', function(req, res) {
    req.session.destroy();
    res.send(true);
});

app.listen(config.port);